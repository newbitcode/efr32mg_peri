
// nb_adc.h

#include "em_iadc.h"



#ifndef NB_ADC_SINGLE_H
#define NB_ADC_SINGLE_H




extern void nb_start_running(void);


extern void nb_adc_single_result_get( IADC_Result_t* adc );

extern void nb_adc_single_result_print(void);


#endif  //NB_ADC_SINGLE_H
