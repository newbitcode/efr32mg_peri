// nb_hal.c

/*
 * GPIO initial
 */

#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_gpio.h"

#include "newbit/nb_hal.h"


// Initial GPIO
void nb_gpio_init(void)
{
	// set as output
	GPIO_PinModeSet(TIMER_USED_GPIO_PORT_0, TIMER_USED_GPIO_PIN_0, gpioModePushPull, 0);


#ifdef TIMER_USED_GPIO_PORT_1
	GPIO_PinModeSet(TIMER_USED_GPIO_PORT_1, TIMER_USED_GPIO_PIN_1, gpioModePushPull, 0);
#endif
}
