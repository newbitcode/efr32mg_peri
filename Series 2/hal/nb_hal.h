

#ifndef NEWBIT_HAL_H
#define NEWBIT_HAL_H

/*
#define TIMER0_GPIO_PORT		gpioPortC
#define TIMER0_GPIO_PIN		3

#define TIMER1_GPIO_PORT		gpioPortC
#define TIMER1_GPIO_PIN		3
*/

#define TIMER_USED_GPIO_PORT_0		gpioPortC
#define TIMER_USED_GPIO_PIN_0		  3

#define TIMER_USED_GPIO_PORT_1		gpioPortC
#define TIMER_USED_GPIO_PIN_1		   2


#define ADC_INPUT_CHANLE_PORT				gpioPortC
#define ADC_INPUT_CHANLE_PIN				0



//which timer
#define TIMER_USED								TIMER1
// clock of this timer
#define CMU_CLOCK_TIMER_USED 		cmuClock_TIMER1


// GPIO Register
//#if  (TIMER_USED==TIMER1)
#define TIMER_INDEX_0  1
//#define TIMER_INDEX_1  1
//#define TIMER_INDEX_2  2
//#else
//#define TIMER_INDEX	0
//#endif

extern void nb_gpio_init(void);

#endif
